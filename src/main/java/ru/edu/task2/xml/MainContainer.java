package ru.edu.task2.xml;

/**
 * ReadOnly
 */
public class MainContainer {

    private TimeKeeper keeper;

    private Child child;

    public MainContainer(TimeKeeper keeperBean, Child childBean) {
        keeper = keeperBean;
        child = childBean;
    }

    public boolean isValid() {
        if(keeper == null || child.getTimeKeeper() == null){
            throw new RuntimeException("Есть пустая зависимость");
        }
        if(keeper.equals(child.getTimeKeeper())){
            throw new RuntimeException("Зависимости не должны совпадать");
        }
        return true;
    }
}
